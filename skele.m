function X = skele(A)
[m,n] = size(A);
flag = 1;
k = 0;
neiBall = [0 1 0; 1 1 1; 0 1 0];
X = zeros(m,n);
while flag == 1 && k<m/2;
    k = k+1;
    %make ball of radius k, center k+1,k+1
    B = zeros(2*k+1,2*k+1);
    x = k+1;
    y = k+1;
    for i = -k:k,
        for j = -k:k,
            if i^2 + j^2 <= k
                B(x+i,y+j) = 1;
            end
        end
    end
    
    %make subSkel & test for emptiness & do union
    nec1 = erosion(A,B,x,y);
    prec1 = opening(nec1,neiBall,2,2);
    flag = 0;
    for i = 1:m,
        for j = 1:n,
            if nec1(i,j) == 1 && prec1(i,j) == 0
                flag = 1;
                X(i,j) = 1;
            end
        end
    end
    
end