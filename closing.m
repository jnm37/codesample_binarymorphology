function X = closing(A,B,x,y)
X = erosion(dilation(A,B,x,y),B,x,y);
end