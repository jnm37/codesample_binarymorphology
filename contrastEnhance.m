function G = contrastEnhance(F,maxBright)
%contrastEnhance implements histogram equalization
%   F should be an a graylevel image with range integers between 0
%   and maxBright
[m,n] = size(F);
N = m*n;
G = zeros(m,n);
Fmod = zeros(m,n);%F + ones(m,n);%MATLAB is having data type issues!
for i = 1:m,
    for j = 1:n,
        Fmod(i,j) = F(i,j) + 1;
    end
end
H = bHistogram(Fmod,maxBright+1);
T = zeros(1,maxBright+1);
for r = 1:maxBright+1,
    for j = 1:r,
        T(r) = T(r) + maxBright*H(j)/N;
    end
end
for i = 1:m,
    for j = 1:n,
        G(i,j) = T(F(i,j)+1)-1;
    end
end
end

