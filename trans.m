function X = trans(A,x,y)
%Assumes A is zero everywhere outside the matrix
[m,n] = size(A);
X = zeros(m,n);
for i = 1:m,
    for j = 1:n,
        if i+x<=m && i+x>=1 && j+y<=n && j+y >=1
            X(i+x,j+y) = A(i,j);
        end
    end
end
end