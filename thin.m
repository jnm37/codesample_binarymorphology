function A = thin(A)
L{1} = [0 0 0; 2 1 2; 1 1 1];
L{2} = [1 2 0; 1 1 0; 1 2 0];
L{3} = [1 1 1; 2 1 2; 0 0 0];
L{4} = [0 2 1; 0 1 1; 0 2 1];
L{5} = [2 1 2; 1 1 0; 2 0 0];
L{6} = [2 1 2; 0 1 1; 0 0 2];
L{7} = [0 0 2; 0 1 1; 2 1 2];
L{8} = [2 0 0; 1 1 0; 2 1 2];

[m,n] = size(A);
Aold = A; %initialize to proper size
changeFlag = 1;
while changeFlag == 1;
    for k = 1:8,
        nec1 = A;
        prec1 = hitMiss(A,L{k},2,2);
        A = zeros(m,n);
        for i = 1:m,
            for j = 1:n,
                if nec1(i,j) == 1 && prec1(i,j) == 0
                    A(i,j) = 1;
                end
            end
        end
    end
    
    changeFlag = 0;
    for i = 1:m,
        for j = 1:n,
            if A(i,j) ~= Aold(i,j)
                changeFlag = 1;
            end
        end
    end
    Aold = A;
end



end