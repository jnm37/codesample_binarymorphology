function X = geoEro(marker,mask,struct,x,y)
%binary geodesic erosion
%marker must be the same size as mask
%struct(x,y) corresponds to origin

[m,n] = size(marker);
X = marker;
Xold = marker;
changeFlag = 1;
while changeFlag == 1,
    X = GE1(X,mask,struct,x,y);
    changeFlag = 0;
    for i = 1:m,
        for j = 1:n,
            if X(i,j) ~= Xold(i,j)
                changeFlag = 1;
            end
        end
    end
    Xold = X;
end
end

function X = GE1(marker,mask,struct,x,y)
suf1 = erosion(marker,struct,x,y);
suf2 = mask;
[m,n] = size(mask);
X = zeros(m,n);
for i = 1:m,
    for j = 1:n,
        if suf1(i,j) == 1 || suf2(i,j) == 1
            X(i,j) = 1;
        end
    end
end
end