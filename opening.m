function X = opening(A,B,x,y)
%Author: Jason Morrow
%Last Edited: 12/26/2016
%Morphological opening on A with structuring element B
X = dilation(erosion(A,B,x,y),B,x,y);
end