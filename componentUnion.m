function blob = componentUnion(list,numobjects,m,n)

blob = zeros(m,n);
for k = 1:numobjects
    blob(list{k}) = 1;
end
    
end