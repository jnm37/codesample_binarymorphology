function X = erosion(A,B,x,y)
%Author: Jason Morrow
%Last Edited: 12/26/2016
%A is the binary image to be transformed
%B is the structuring element
%B(x,y) corresponds to origin
[m,n] = size(B);
[u,v] = size(A);
X = ones(u,v);
for i = 1:m,
    for j = 1:n,
        if B(i,j) == 1
            Ashift = trans(A,x-i,y-j);
            for g = 1:u,
                for h = 1:v,
                    if Ashift(g,h) == 0
                        X(g,h) = 0;
                    end
                end
            end
        end
    end
end


end