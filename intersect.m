function X = intersect(A,B)
[m,n] = size(A);
X = zeros(m,n);
for i = 1:m,
    for j = 1:n,
        if A(i,j) == 1 && B(i,j) == 1
            X(i,j) = 1;
        end
    end
end

end

