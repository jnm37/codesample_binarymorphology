function H = bHistogram(P,g)
%This function creates a brightness histogram for image matrix P assuming g
%distinct gray levels
%the elements of P should be integers from 1 to g
[M,N] = size(P);
H = zeros(1,g);
for m = 1:M,
    for n = 1:N,
        H(P(m,n)) = H(P(m,n)) + 1;
    end
end


end

