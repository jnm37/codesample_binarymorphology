function step = bfs(mask,startx,starty,endx,endy)
%bredth first search applied to an image

[m,n] = size(mask);
step = 0;
seed = zeros(m,n);
seed(startx,starty) = 1;
B = ones(3,3); %neighborhood
x = 2;
y = 2;
temp = zeros(m,n);
while seed(endx,endy) == 0 && ~ isequal(temp,seed)
    temp = seed;
    step = step + 1;
    seed = dilation(seed, B, x ,y);
    seed = intersect(seed,mask);
end

end

