function X = geoDil(marker,mask,struct,x,y)
%binary geodesic dilation
%marker must be the same size as mask
%struct(x,y) corresponds to origin

[m,n] = size(marker);
X = marker;
Xold = marker;
changeFlag = 1;
while changeFlag == 1,
    X = GD1(X,mask,struct,x,y);
    changeFlag = 0;
    for i = 1:m,
        for j = 1:n,
            if X(i,j) ~= Xold(i,j)
                changeFlag = 1;
            end
        end
    end
    Xold = X;
end
end

function X = GD1(marker,mask,struct,x,y)
nec1 = dilation(marker,struct,x,y);
nec2 = mask;
[m,n] = size(mask);
X = zeros(m,n);
for i = 1:m,
    for j = 1:n,
        if nec1(i,j) == 1 && nec2(i,j) == 1
            X(i,j) = 1;
        end
    end
end
end