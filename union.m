function X = union(A,B)
[m,n] = size(A);
X = ones(m,n);
for i = 1:m,
    for j = 1:n,
        if A(i,j) == 0 && B(i,j) == 0
            X(i,j) = 0;
        end
    end
end

end

