function blur = squintSmooth(f)
[m,n] = size(f);
% bino = [1; 6; 15; 20; 15; 6; 1];
% lambda = 20;
% stencil = 1/(42^2+lambda*7^2)*(bino*bino'+lambda*ones(7,7));
marg = 5;
stencil = 1/(1+2*marg)^2*ones(1+2*marg,1+2*marg);
blur = zeros(m,n);
for i = 1+marg:m-marg
    for j = 1+marg:m-marg
        foo = f(i-marg:i+marg,j-marg:j+marg);
        foo = foo.*stencil;
        bar = foo(:);
        blur(i,j) = sum(bar);
    end
end

end