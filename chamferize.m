%for this demonstration of the function, dist=8 and P = [0 0 0 0 0; 0 1 0 0 0; 0 0 0 1 0; 0 0 0 0 0]
function G = chamferize(P,dist)
%This function performs the Distance Transform on a binary image
%represented as matrix P
%   P should be a non-zero matrix containing only zeros and ones. dist should be 2
%   for Euclidean, 4 for Manhattan, or 8 for Chessboard.
    
    %This function returns the distance between point x and point y
    function distance = d(x,y)
        if dist == 2
            %Euclidean
            distance = sqrt((x(1)-y(1))^2 +(x(2)-y(2))^2);
        elseif dist == 4
            %Manhattan
            distance = abs(x(1)-y(1)) + abs(x(2)-y(2));
        else
            %Chessboard
            distance = max(abs(x(1)-y(1)),abs(x(2)-y(2)));
        end
    end

%initialize F to be zero where P is one and large elsewhere
%also put a border of gigantic numbers around it
[M,N] = size(P);
F = zeros(M+2,N+2);
for m = 1:M,
    for n = 1:N,
        if P(m,n) == 0
            F(m+1,n+1) = M + N +4; %close enough to infinity
        end
    end
end
for m = 1:M+2,
    F(m,1) = 2*M + 2*N + 8;
    F(m,N+2) = 2*M + 2*N +8;
end
for n = 1:N+2,
    F(1,n) = 2*M + 2*N +8;
    F(M+2,n) = 2*M + 2*N + 8;
end

%AL mask sweep
for m = 2:M+1,
    for n = 2:N+1,
       foo = zeros(1,5); %intialize the variable to the size I need
       foo(1) = F(m,n);
       foo(2) = F(m-1,n)+d([m n],[m-1 n]);
       foo(3) = F(m-1,n-1)+d([m n],[m-1 n-1]);
       foo(4) = F(m,n-1)+d([m n],[m n-1]);
       foo(5) = F(m+1,n-1)+d([m n],[m+1,n-1]);
       F(m,n) = min(foo);
    end
end

%BR mask sweep
for m = M+1:-1:2,
    for n = N+1:-1:2,
       foo = zeros(1,5); %intialize the variable to the size I need
       foo(1) = F(m,n);
       foo(2) = F(m+1,n)+d([m n],[m+1 n]);
       foo(3) = F(m+1,n+1)+d([m n],[m+1 n+1]);
       foo(4) = F(m,n+1)+d([m n],[m n+1]);
       foo(5) = F(m-1,n+1)+d([m n],[m-1,n+1]);
       F(m,n) = min(foo);
    end
end

%get rid of the border
G = zeros(M,N);
for m = 1:M,
    for n = 1:N,
        G(m,n) = F(m+1,n+1);
    end
end
end

